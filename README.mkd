# tmux with powerline setup 
+ clone the repo to ```~/.tmux``` or make symbolic link with ```ln -sf /your/path/my-tmux ~/.tmux```

+ replace local tmux config file with the one in my-tmux: ```ln -sf ~/.tmux/tmux.conf ~/.tmux.conf```
+ prefix is C-a (CTRL-a)
+ once done, start tmux and do prefix+I
